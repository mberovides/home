﻿function GetJSONData(hiddenValue, formID) {
    $("#verNome").val(hiddenValue);
    var form = $('#' + formID)
    $.ajax({
        url: form.attr('action'),
        type: "POST",
        data: form.serialize(),
        success: function (response) {
            if (hiddenValue == 'JSON') {
                // parseJSON
                response = $.parseJSON(response);
                //Pega a div e chama o método knockout
                var dn = document.getElementById('jsonDiv');
                var x = ko.contextFor(dn);
                x.$root.getClientes(response);
            }
        },
        error: function (error) {
            alert(error.status + "<--e--> " + error.statusText);
        }
    });
}

function Cliente(pnome, pemail) {
    this.Nome = pnome;
    this.Email = pemail;
}

function clienteVM() {
    var self = this;
    self.Nome = ko.observable();
    self.Email = ko.observable();
    self.clientes = ko.observableArray([]);

    self.getClientes = function (data) {
        self.clientes.removeAll();
        $.each(data, function (key, val) {
            self.clientes.push(new Cliente(val.Nome, val.Email));
        });
    };
}
$(document).ready(function () {
    var dn = document.getElementById('jsonDiv');
    ko.applyBindings(new clienteVM(), dn);
});
