﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MediaSolutionTest.FrontEnd.Startup))]
namespace MediaSolutionTest.FrontEnd
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
